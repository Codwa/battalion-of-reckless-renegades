/*
 * @author Jonathan Heider
 */


using UnityEngine;
using System.Collections;

public class WeaponLogic : MonoBehaviour {
	
	public byte activeWeapon;
	public byte weapon;
	private float nextFire=0.3f;
	private float fireCount=0;
	public int weaponState;//0= both 1= first 4 bits    2= last 4 bits
	public GameObject projectile;
	public GameObject lowerWave;//wave particle that goes down first
	public GameObject gun;
	public bool pickingUp;
	// Use this for initialization
	void Start () {
		weapon=1;// should change to a config or something to maintain state from "level" to level
		activeWeapon=weapon;
		projectile=Resources.Load(checkWeaponType(activeWeapon))as GameObject;
		lowerWave=Resources.Load("lowerWave")as GameObject;
		pickingUp=false;
	}
	
	// Update is called once per frame
	
	
	/*weapon key
	 * 00000001 bullet first slot       1
	 * 00000010 fire first slot		    2
	 * 00000100 wave first slot			4
	 * 00001000 pen first slot		    8
	 * 00010000 bullet second slot		16
	 * 00100000 flame second slot		32
	 * 01000000 wave second slot		64
	 * 10000000 pen second slot			128
	 */
	void Update () {
		if(Time.timeScale!=0){
		if(Input.GetButton("Fire1") && Time.time>nextFire){

		if(Input.GetButton("Fire1") && fireCount>nextFire){

			GameObject clone=Instantiate(projectile,gun.transform.position,gun.transform.rotation) as GameObject as GameObject;
			if(checkWeaponType(activeWeapon)=="double helix"){
				GameObject wave=Instantiate(lowerWave,gun.transform.position,gun.transform.rotation) as GameObject as GameObject;

			}
			

			fireCount = 0;

		}
		
		else{
			fireCount += Time.deltaTime;
				}
		
		}
			if(Input.GetButtonDown("wep")){
					
			nextActiveWeapon();	
		}
	
	}
	}
	/*
	 * @name check weapon type
	 * @param the weapon that you want to check the value of
	 * @desc  determines the weapon type of the given weapon intended for easy debugging
	 * @return  returns a string with the weapon type
	 * 
	 * */
	string checkWeaponType(byte wep){
		switch(wep){
		case 1:case 16:
			return "bullet";
		case 17:
			return "super bullet";
		case 2:case 32:
			return "fire";
		case 18:case 33:
			return "explosive round";
		case 34:
			return "super fire";
		case 4: case 64:
			return "wave";
		case 65:case 20:
			return "ricochet";
		case 66: case 36:
			return "fire wave";
		case 68:
			return "double helix";
		case 8: case 128:
			return "pen";
		case 129:case 24:
			return "pen bullet";
		case 130: case 40:
			return "flame pen";
		case 132: case 72:
			return "wave pen";
		case 136:
			return "double pen";
			
		default:
			return "Invalid weapon";
		}
	
	}
	
	/*
	 * @name next active weapon
	 * @desc cycles between the the 2 weapon capsules and both
	 * 
	 */
	void nextActiveWeapon(){
		weaponState=(weaponState+1)%3;
		if(weaponState==0){
			activeWeapon=weapon;
		}
		else if(weaponState==1){
			activeWeapon= (byte)(weapon& 15);
		}
		else if(weaponState==2){
			if(weapon>>4 != 0){// to check if only one weapon is obtained
			activeWeapon= (byte)(weapon>>4);
			}
		}
		projectile= Resources.Load(checkWeaponType(activeWeapon))as GameObject;
	}
	
	/* @name weapon pickup
	 * @param the value of a solo weapon sent from a pickup
	 * @desc picks up a weapon and combines it with the active weapon
	 * 
	 * */
	public void weaponPickup(byte pickup){
		activeWeapon = (byte)(activeWeapon<<4);
		activeWeapon = (byte)(activeWeapon|pickup);
		weapon = activeWeapon;
		
		projectile= Resources.Load(checkWeaponType(activeWeapon))as GameObject;
	}
	
	/*
	 * @name is picking up
	 * @return whether or not another pick up has reserved the pickup function
	 * @desc checks to see if anything is calling the weapon pickup function. if nothin
	 * is the function is locked
	 */
	public bool isPickingUp(){
		lock(this){
		if(!pickingUp){
			pickingUp=true;
			return false;
		}
		else{
			return true;
		}
		}
	}
	
	/*@name done Picking
	 *@desc unlocks the weapon pick up function
	 * 
	 */
	public IEnumerator donePicking(){
		Debug.Log("starting");
		yield return new WaitForSeconds(1);
		pickingUp=false;
		
	}
}
