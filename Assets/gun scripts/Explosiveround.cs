/*
 * @author JOnathan Heider
 */
using UnityEngine;
using System.Collections;

public class Explosiveround : basicBullet
{
	public float speed= 3;
	public float damage=3;
	public float lifetime = 5;
	GameObject explosion;
	//int test=0;
	// Use this for initialization
	void Start ()
	{
		speed=20; 
		damage=3;
		explosion=Resources.Load("fire") as GameObject;
		Destroy(gameObject,lifetime);
	
	}
	
	// Update is called once per frame
	void Update ()
	{
			transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	//	    if(test==200){
	//		explode ();
	//	}
	//	test++;
	}
	
	void explode(){//should be called on collision
		GameObject forward=Instantiate(explosion,transform.position,transform.rotation) as GameObject as GameObject;
		GameObject up=Instantiate(explosion,transform.position,new Quaternion(0,180,180,1)) as GameObject as GameObject;
		GameObject back=Instantiate(explosion,transform.position,new Quaternion(0,0,180,1)) as GameObject as GameObject;
		GameObject down=Instantiate(explosion,transform.position,new Quaternion(0,180,-180,1)) as GameObject as GameObject;
		Destroy(gameObject);
	}
	public void hit(){
	explode();
	}
	public override float damageHit() { 
		explode ();
		return damage; 
	}
}

