//File: 		Main Character Control
//Purpose:		Functionality of the player character
//Written by: 	Cody Way
//Created on: 	7/10/13
//Last updated: 7/16/13

using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	
	public float acceleration;
	public float jumpSpeed;
    public float gravity;
	
	private Vector3 moveDirection;
	private bool facingRight;
	private bool prevFacing;

	// Use this for initialization
	void Start () {
		moveDirection = Vector3.zero;
		facingRight = true;
		prevFacing = facingRight;
	}
	
	// Update is called once per frame
	void Update () {
		CharacterController controller = GetComponent<CharacterController>();
		
		//Movement on ground
		if(controller.isGrounded) 
		{
			//Calculate normal movement
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            //moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= acceleration;
			
			//Check for jumping
            if (controller.isGrounded && (Input.GetButton("Jump")) || (Input.GetAxis("Vertical") == 1))
                moveDirection.y = jumpSpeed;
			
			//Check facing of character
			if(moveDirection.x > 0)
				facingRight = true;
			else if(moveDirection.x < 0)
				facingRight = false;
        }
		
		//Movement while in-air
		else
		{
			//Calculate the Y moveDirection, and use it after X moveDirection is determined
			float holderY = moveDirection.y - (gravity * Time.deltaTime);
			
			//Calculate normal X movement
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            //moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= acceleration;
			
			//Replace Y value
			moveDirection.y = holderY;
			
			//Check facing of character
			if(moveDirection.x > 0)
				facingRight = true;
			else if(moveDirection.x < 0)
				facingRight = false;
				
		}
		
		//Change rotation of character
		if(facingRight != prevFacing) {
			controller.transform.Rotate(0,180,0,Space.Self);
			prevFacing = facingRight;
		}
		
		//If character is facing left, flip the value (due to the rotation of the object)
		//if(!facingRight)
			//moveDirection.x *= -1;
		
		//Move character
        controller.Move(moveDirection * Time.deltaTime);
		
		//Ensure player stays on correct plane
		if(controller.transform.position.z != -4)
			controller.transform.position = new Vector3(controller.transform.position.x, controller.transform.position.y, -4);
	}
}
