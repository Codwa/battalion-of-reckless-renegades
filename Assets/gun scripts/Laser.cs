/*
 * @author Jonahan HEider
 */
using UnityEngine;
using System.Collections;

public class Laser : basicBullet
{

	public float speed= 3;
	public float damage=4;
	public float lifetime = 5;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){
		if(damage>1){
			damage-=1;
		}
	}
	public override float damageHit() { return damage; }
}

