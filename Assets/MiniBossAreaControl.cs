//File: 		Camera Control
//Purpose:		Pass boss camera location to camera so it can move properly and information concerning how enemies should spawn
//Written by: 	Cody Way
//Created on: 	7/23/13
//Last updated: 8/7/13

using UnityEngine;
using System.Collections;
using SpawnTypes;

namespace SpawnTypes {
	public struct BossAreaMessageParams {
		public Vector3 pos;
		public SpawnState state;
	}
}

public class MiniBossAreaControl : MonoBehaviour {

	void OnTriggerEnter (Collider player) {
 		//Check to see if player has entered area
		if(player.CompareTag("Player")) {
			//Get camera component and camera-position
			Transform obj = transform.FindChild("CameraPosition");
			Vector3 position = transform.TransformDirection(obj.position);
			SpawnState state;
			
			if(this.tag.ToString() == "LeftSpawnOnly")
				state = SpawnState.LEFT;
			else if(this.tag.ToString() == "RightSpawnOnly")
				state = SpawnState.RIGHT;
			else if(this.tag.ToString() == "NoSpawn")
				state = SpawnState.NONE;
			else
				state = SpawnState.NORMAL;
			
			BossAreaMessageParams result;
			result.pos = position;
			result.state = state;
			
			Camera.mainCamera.SendMessage("EnterBossArea", result);
		}
	}

 

	void OnTriggerExit (Collider player) {
	    //Check to see if player has exited area
		if(player.CompareTag("Player")) {
			//Get camera component
			GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
			
			cam.SendMessage("ExitBossArea");
		}
	}
}
