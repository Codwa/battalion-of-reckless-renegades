//File: 		Destructible enviroment
//Purpose:		Allows an object to take damage until it reachs 0, which will then bring about destruction
//Written by: 	Cody Way
//Created on: 	7/24/13
//Last updated: 7/26/13

using UnityEngine;
using System.Collections;

public class DestructibleEnviro : MonoBehaviour {
	
	public float health;

	void OnCollisionEnter(Collision collide) {
		if(collide.gameObject.CompareTag("Bullet")) {
			health = health - collide.gameObject.GetComponent<basicBullet>().damageHit();
			collide.gameObject.SendMessage("hit");
			
			if(health <= 0) {
				gameObject.rigidbody.freezeRotation = false;
				gameObject.rigidbody.mass = 0.0001f;
				gameObject.rigidbody.AddForce(new Vector3(Random.value, Random.value, Random.value));
				Destroy(gameObject, 6);
			}
		}
	}
}
