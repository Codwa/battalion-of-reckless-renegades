/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class LowWave : basicBullet
{public float CurveSpeed;
	public float speed;
	public int count=0;
	public float damage=2;
	public float lifetime = 5;
    //public float MoveSpeed = 1;
 
    float fTime = 0;
    Vector3 vLastPos = Vector3.zero;
	// Use this for initialization
	void Start () {
		CurveSpeed=  15;
		speed=15;
	Destroy(gameObject,lifetime);
	}
	
	
	// Update is called once per frame
	void Update () {
		vLastPos = transform.position;
 
       fTime += Time.deltaTime * CurveSpeed;
 
       Vector3 vSin = new Vector3(0, 20*Mathf.Cos(fTime), 0);
       Vector3 vLin = transform.forward*speed;
 
       transform.position += (vSin + vLin) * Time.deltaTime;
 
       Debug.DrawLine(vLastPos, transform.position, Color.green, 100);
		}
		
	public void hit(){
		Destroy(gameObject);
	}
	public override float damageHit() { return damage; }
}

