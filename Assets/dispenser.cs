/*
 * @author Jonathan Heider
 */

using UnityEngine;
using System.Collections;

public class dispenser : MonoBehaviour
{
	public int stock=2;
	public GameObject spawn;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnTriggerStay(Collider player)
	{
		if(player.CompareTag("Player") && 
						Input.GetButtonDown("Fire1") && 
							Input.GetAxis("Vertical")<0){
			stock-=1;
		switch(Random.Range(0,100)%4){
				case 0:
					GameObject bul=Instantiate(Resources.Load("bulletpickup")as GameObject,spawn.transform.position,transform.rotation)as GameObject;
				break;
			case 1:
				GameObject fire=Instantiate(Resources.Load("firePickup")as GameObject,spawn.transform.position,transform.rotation)as GameObject;
			break;
			case 2:
				GameObject pen=Instantiate(Resources.Load("penpickup")as GameObject,spawn.transform.position,transform.rotation)as GameObject;
			break;
			case 3:
				GameObject wave=Instantiate(Resources.Load("wavepickup")as GameObject,spawn.transform.position,transform.rotation)as GameObject;
			break;
			}
			if(stock<=0){
				Destroy(gameObject);
			}
		}
	}
}

