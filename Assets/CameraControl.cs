//File: 		Camera Control
//Purpose:		Moves the camera to follow the player, move to boss areas, and handle's spawning enemies from the sides.
//Written by: 	Cody Way
//Created on: 	7/16/13
//Last updated: 8/7/13

using UnityEngine;
using System.Collections;
using SpawnTypes;

namespace SpawnTypes {
	public enum SpawnState { NORMAL, LEFT, RIGHT, NONE } ;
}

public class CameraControl : MonoBehaviour {
	
	//Camera position variables
	public float cameraOffsetX1;
	public float cameraOffsetX2;
	public float cameraOffsetY;
	public float cameraOffsetZ;
	public Vector3 bossPosition;
	
	//Boss Camera variables
	public bool bossActive;
	private bool prevBoss;
	private bool bossTravel;
	
	//Following Player variables
	private bool facingRight;
	private bool prevFacing;
	private bool traveling;
	
	//Lerp variable
	private float time;
	
	//Enemy Spawn variables
	public GameObject leftSpawn;
	public GameObject rightSpawn;
	private SpawnState state;
	private float spawnTime;
	public float spawnTimer;
	public GameObject PrimaryEnemy;

	//Use this for initialization
	void Start () {
		traveling = false;
		time = 0;
		facingRight = true;
		prevFacing = facingRight;
		
		bossActive = false;
		prevBoss = bossActive;
		bossTravel = false;
		spawnTime = 0;
	}
	
	//Update is called once per frame
	void Update () {
		Camera camera = GetComponent<Camera>();
		Transform player = GameObject.FindGameObjectWithTag("Player").transform;
		
		#region Movement and Facing
		//Determine facing of the character
		if(Input.GetAxis("Horizontal") == 1)
			facingRight = true;
		else if (Input.GetAxis("Horizontal") == -1)
			facingRight = false;
		
		//Determine whether or not camera needs moved
		if(facingRight != prevFacing)
		{
			traveling = true;
			prevFacing = facingRight;
			time = 0;
		}
		if(bossActive != prevBoss)
		{
			bossTravel = true;
			prevBoss = bossActive;
			time = 0;
		}
		
		//Moves camera to boss area camera location
		if(bossTravel)
		{
			if(bossActive)
				camera.transform.position = Vector3.Lerp(camera.transform.position,bossPosition,time);
			else {
				if(facingRight) 
					camera.transform.position = Vector3.Lerp(camera.transform.position,new Vector3(player.position.x + cameraOffsetX1, cameraOffsetY, cameraOffsetZ),time);
				else 
					camera.transform.position = Vector3.Lerp(camera.transform.position,new Vector3(player.position.x + cameraOffsetX2, cameraOffsetY, cameraOffsetZ),time);
			}
			
			time += Time.deltaTime;
			if(time >= 1)
				bossTravel = false;
		}
		
		//Moves camera to left or right side
		else if(traveling && !bossActive)
		{
			if(facingRight) 
				camera.transform.position = Vector3.Lerp(camera.transform.position,new Vector3(player.position.x + cameraOffsetX1, cameraOffsetY, cameraOffsetZ),time);
			else 
				camera.transform.position = Vector3.Lerp(camera.transform.position,new Vector3(player.position.x + cameraOffsetX2, cameraOffsetY, cameraOffsetZ),time);
				
			time += Time.deltaTime;
			if(time >= 1)
				traveling = false;
		}
		
		//Follow player
		else if(!bossActive)
		{
			if(facingRight)
				camera.transform.position = new Vector3(player.position.x + cameraOffsetX1, cameraOffsetY, cameraOffsetZ);
			else
				camera.transform.position = new Vector3(player.position.x + cameraOffsetX2, cameraOffsetY, cameraOffsetZ);
		
		}
		
		#endregion
		
		if(Time.timeScale != 0) {
			spawnTime += Time.deltaTime;
			if(spawnTime >= spawnTimer) {
				Transform obj;
				Vector3 position;
				
				switch(state) {
				case SpawnState.LEFT:
					obj = transform.FindChild("LeftSpawn");
					Instantiate(PrimaryEnemy,obj.position,obj.rotation);
					break;
					
				case SpawnState.RIGHT:
					obj = transform.FindChild("RightSpawn");
					Instantiate(PrimaryEnemy,obj.position,obj.rotation);
					break;
					
				case SpawnState.NORMAL:
					obj = transform.FindChild("RightSpawn");
					Instantiate(PrimaryEnemy,obj.position,obj.rotation);
					
					obj = transform.FindChild("LeftSpawn");
					Instantiate(PrimaryEnemy,obj.position,obj.rotation);
					break;
				}
				
				spawnTime = 0;
			}
		}
	}
	
	//Call when entering boss area
	void EnterBossArea(BossAreaMessageParams values) {
		bossPosition = values.pos;
		bossActive = true;
		state = values.state;
	}
	
	//Call when exiting boss area
	void ExitBossArea() {
		bossActive = false;	
		state = SpawnState.NORMAL;
	}
}
