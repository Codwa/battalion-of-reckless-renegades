//File: 		TopPlatformTrigger
//Purpose:		Allows the player to fall through platforms
//Written by: 	Cody Way
//Created on: 	7/23/13
//Last updated: 7/24/13

using UnityEngine;
using System.Collections;

public class TopPlatformTrigger : MonoBehaviour {

	void Update () {
		if(Input.GetAxis("Vertical") < 0) {
	 		//Get the parent platform and the player
			GameObject controller = GameObject.FindGameObjectWithTag("Player");
			Transform platform = transform.parent;
			
			//Check to see if they intersect 
			if(this.collider.bounds.Intersects(controller.collider.bounds)) {
				Physics.IgnoreCollision(controller.GetComponent<Collider>(), platform.GetComponent<BoxCollider>());
			}
		}
	}

 

	void OnTriggerExit (Collider jumper) {
	    //Re-enable collision
		if(jumper.CompareTag("Player") || jumper.CompareTag("Enemy")) {
    		Transform platform = transform.parent;
    		Physics.IgnoreCollision(jumper.GetComponent<CharacterController>(), platform.GetComponent<BoxCollider>(), false);
		}
	}
}
