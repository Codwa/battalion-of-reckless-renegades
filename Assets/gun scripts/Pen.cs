/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class Pen : basicBullet
{

	public float speed= 1;
	public float damage=1;
	public float lifetime = 5;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {//may need to put something here to ignore collision
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){
	}
	public override float damageHit() { return damage; }
}

