//File: 		BottomPlatformTrigger
//Purpose:		Allows the player to jump up through platforms
//Written by: 	Cody Way
//Created on: 	7/23/13
//Last updated: 7/24/13

using UnityEngine;
using System.Collections;


public class BottomPlatformTrigger : MonoBehaviour {

	void OnTriggerEnter (Collider jumper) {
 		//Make platform ignore jumper
		if(jumper.CompareTag("Player") || jumper.CompareTag("Enemy")) {
			Transform platform = transform.parent;
			Physics.IgnoreCollision(jumper.GetComponent<CharacterController>(), platform.GetComponent<BoxCollider>());
		}
	}

 

	void OnTriggerExit (Collider jumper) {
	    //Re-enable collision
		if(jumper.CompareTag("Player") || jumper.CompareTag("Enemy")) {
	    	Transform platform = transform.parent;
	    	Physics.IgnoreCollision(jumper.GetComponent<CharacterController>(), platform.GetComponent<BoxCollider>(), false);
		}
	}
}
