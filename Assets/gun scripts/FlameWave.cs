/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class FlameWave : basicBullet
{
public float CurveSpeed;
	public float speed;
	public int count=0;
	public float damage=4;
	public float lifetime = 1;
    //public float MoveSpeed = 1;
 
    float fTime = 0;
    Vector3 vLastPos = Vector3.zero;
	// Use this for initialization
	void Start () {
		CurveSpeed=  15;
		speed=10;
		Destroy(gameObject,lifetime);
	}
	
	
	// Update is called once per frame
	void Update () {
		vLastPos = transform.position;
 
       fTime += Time.deltaTime * CurveSpeed;
 
       Vector3 vSin = new Vector3(0, -20*Mathf.Cos(fTime), 0);
       Vector3 vLin = transform.forward*speed;
 
       transform.position += (vSin + vLin) * Time.deltaTime;
 
		}
		
	public void hit(){
		
	}
	public override float damageHit() { return damage; }
}

