/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	int paused;
	// Use this for initialization
	void Start () {
		paused=1;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonUp("pause")){
			paused+=1;
			paused= paused%2;
			Time.timeScale=paused;
		}
	}
}
