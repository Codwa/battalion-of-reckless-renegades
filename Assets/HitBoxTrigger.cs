using UnityEngine;
using System.Collections;

public class HitBoxTrigger : MonoBehaviour {
	
	public BasicAI parent;

	void OnTriggerEnter(Collider bullet) {
		if(bullet.CompareTag("Bullet")) {
			parent.TakeDamage(bullet);
		}
	}
}
