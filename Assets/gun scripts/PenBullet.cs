/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class PenBullet : basicBullet
{

	public float speed= 3;
	public float damage=5;
	public float lifetime = 5;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){
		if(damage==5){
			damage=3;
		}
	}
	public override float damageHit() { 
		float dam=damage;
		hit();
		return dam; 
	}
}

