/*
 * @author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class Bullet : basicBullet {

	public float speed= 3;
	public float damage=3;
	public float lifetime = 5;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){
		Destroy(gameObject);
	}
	public override float damageHit() { return damage; }
}
