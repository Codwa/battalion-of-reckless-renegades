/*
 *@author Jonathan Heider 
 * 
 */

using UnityEngine;
using System.Collections;

public class penpickup : MonoBehaviour {

	public byte val;

	void Start () {
		val=8;
	}
	
	// Update is called once per frame
	void OnTriggerStay(Collider player){
		if(player.CompareTag("Player") && 
						Input.GetButtonDown("Fire1") && 
							Input.GetAxis("Vertical")<0){
			
			WeaponLogic other=(WeaponLogic) player.GetComponent(typeof (WeaponLogic));
			if(!other.isPickingUp()){
				other.weaponPickup(val);
				other.StartCoroutine(other.donePicking());
				Destroy(gameObject);
			}
		}
	}
}
