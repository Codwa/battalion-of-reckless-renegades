/*
 *@author Jonathan Heider
 */
using UnityEngine;
using System.Collections;

public class Ricochet : basicBullet
{

	public float speed= 3;
	public float damage=4;
	public float lifetime = 5;
	public int test;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {
		
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){
	}
	public override float damageHit() { return damage; }
}

