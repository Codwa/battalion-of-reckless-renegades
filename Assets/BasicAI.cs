//File: 		Basic AI
//Purpose:		Dictates basic AI Functionality of chasing the player and shooting
//Written by: 	Cody Way
//Created on: 	8/7/13
//Last updated: 8/7/13

using UnityEngine;
using System.Collections;

public class BasicAI : MonoBehaviour {

	public float acceleration;
	public float jumpSpeed;
    public float gravity;
	
	public GameObject player;
	
	private Vector3 moveDirection;
	private bool facingRight;
	private bool prevFacing;
	private CharacterController controller;
	private Transform gun;
	public GameObject bullet;
	
	private int variablity;
	public float distance;
	public int minVariablity;
	public int maxVariablity;
	
	public float health;
	
	public float reloadWaitTime;
	private float reloadTime;
	public float basePercent;
	
	
	void Start() {
		controller = GetComponent<CharacterController>();
		
		if(player.transform.position.x < controller.transform.position.x)
			facingRight = false;
		else
			facingRight = true;
		
		prevFacing = facingRight;		
		moveDirection = Vector3.zero;
		variablity = Random.Range(minVariablity,maxVariablity);
		reloadTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Time.timeScale != 0) {
		
			controller = GetComponent<CharacterController>();
			player = GameObject.FindGameObjectWithTag("Player");
			gun = transform.FindChild("Gun");
			
			#region Movement
			
			#region OnGround
			if(controller.isGrounded) 
			{
				//Calculate normal movement
	            if(Mathf.Abs(player.transform.position.x - controller.transform.position.x) >= (distance *variablity)) {
					if(player.transform.position.x > controller.transform.position.x)
						moveDirection = new Vector3(1,0,0);
					else
						moveDirection = new Vector3(-1,0,0);
				}
				
				else
					moveDirection = Vector3.zero;
				
				moveDirection *= acceleration;
				
				//Decide whether jumping should happen
	            
				
				//Check facing of character
				if(moveDirection.x > 0)
					facingRight = true;
				else if(moveDirection.x < 0)
					facingRight = false;
	        }
			#endregion
			
			#region Airborne
			else
			{
				//Calculate the Y moveDirection, and use it after X moveDirection is determined
				float holderY = moveDirection.y - (gravity * Time.deltaTime);
				
				//Calculate normal X movement
	            if(Mathf.Abs(player.transform.position.x - controller.transform.position.x) >= (distance *variablity)) {
					if(player.transform.position.x > controller.transform.position.x)
						moveDirection = new Vector3(1,0,0);
					else
						moveDirection = new Vector3(-1,0,0);
				}
				
				else
					moveDirection = Vector3.zero;
				
				moveDirection *= acceleration;
				
				//Replace Y value
				moveDirection.y = holderY;
				
				//Check facing of character
				if(moveDirection.x > 0)
					facingRight = true;
				else if(moveDirection.x < 0)
					facingRight = false;
			}
			#endregion
			
			#region Facing
			if(facingRight != prevFacing) {
				controller.transform.Rotate(0,180,0,Space.Self);
				prevFacing = facingRight;
			}
			#endregion
			
			//Move character
	        controller.Move(moveDirection * Time.deltaTime);
			
			//Ensure enemy is on the correct plane
			if(controller.transform.position.z != 4)
				controller.transform.position = new Vector3(controller.transform.position.x, controller.transform.position.y, -4);
			
			#endregion
			
			#region Shooting
			
			gun.LookAt(player.transform.position);
			
			if(reloadTime > reloadWaitTime) {
				reloadTime += Time.deltaTime;
				float percent = Random.value + (reloadTime * (variablity * .1f));
				if(percent >= basePercent) {
					GameObject clone = Instantiate(bullet,gun.position,gun.rotation) as GameObject;
					reloadTime = 0;
				}
			}
			else
				reloadTime += Time.deltaTime;
			
			#endregion
			
		}
	}
	
	public void TakeDamage(Collider collide) {
		health = health - collide.gameObject.GetComponent<basicBullet>().damageHit();
		collide.gameObject.SendMessage("hit");
			
		if(health <= 0)
			Destroy(gameObject);
	}
	
}
