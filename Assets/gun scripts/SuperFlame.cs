/*
 * @author Jonathan Heider
 */

using UnityEngine;
using System.Collections;

public class SuperFlame : basicBullet
{

	public float speed= 1;//determines length of fire
	public float damage=4;
	public float lifetime = 3;
	void Start(){
		Destroy(gameObject,lifetime);
	}
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward*speed*Time.deltaTime,Space.World);
	}
	public void hit(){}
	public override float damageHit() { return damage; }
}

